// router.js
import * as VueRouter from 'vue-router';
import Pick from '../pages/pick.vue'
import Index from '../pages/index.vue'
const routes = [
    { path: '/', component: Index },
    { path: '/pick', component: Pick },

]
  // Vue.config.productionTip = false;
  const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: VueRouter.createWebHashHistory(),
    routes, // short for `routes: routes`
  })
  
  // const defaultTitle = '嘉兴本级疫情3D三区图'
  // router.beforeEach((to, from, next) => {
  //   document.title = to.meta.title ? to.meta.title : defaultTitle;
  //   next()
  // })

  export default router;